<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'codes_equipes_club'=> 'Tous les codes utiles pour les modèles "équipes"',	
	// E
	'evolution_elo'=> 'Evolution du classement',
	// F
	'ffedata_club'=> 'Club',
	'ffedata_club_explication' => 'Le numéro du club dans la base FFE',
	'ffedata_saison' => 'Saison',
	'ffedata_saison_explication' => 'Saison demandée : pour la saison 2014-2015 saisir 2015',
	'ffedata_titre' => 'Data FFE',
	'ffedata_departement'=>'Département',
	'ffedata_saisir_code_departement'=>'saisir le numéro du département',
	'ffedata_donnees_formulaire'=>'les données du webservice dans formidable pour l’inscription',
	// S
	'saisie_nrffe_titre' => 'Numéro de licence FFE',
	'saisie_nrffe_explication' => 'Le numéro de licence',	
	// T
	'titre_page_informations_ffedata' => 'Informations utiles pour utiliser FFE Data',
	'titre_informations_equipes_clubs' => 'Les informations sur les équipes d’un club',
	'titre_informations_clubs_departement'=>'La liste des clubs d’un département',
);