<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'ffedata_description' => 'Ce plugin permet de récupérer les informations stockées sur le webservice de la FFE
	exemples :
- pour le modèle ffe_joueurs : &lt;ffe_joueurs|Ref=XXXX&gt; ou XXXX est le code du club recherché.
- pour le modèle ffe_liste_clubs : &lt;ffe_liste_clubs|Ref=XX&gt; ou XX est le numéro du département recherché.
	',
	'ffedata_nom' => 'Data FFE',
	'ffedata_slogan' => 'Les infos du webservice de la Fédération Française des Echecs',
);